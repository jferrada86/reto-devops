IP=$(docker inspect $(docker ps | grep kind-control-plane | awk '{print $1}') | jq -r '.[].NetworkSettings.Networks.kind.IPAddress')
kubectl patch svc ingress-nginx-controller -p "{\"spec\": {\"type\": \"LoadBalancer\", \"externalIPs\":[\"$IP\"]}}"
