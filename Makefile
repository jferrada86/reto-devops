SHELL = /bin/bash

unit:
	npm install && npm run test

Reto1: Reto1-deploy

Reto1-deploy: docker-start

Reto1-remove: docker-clean

docker-start: docker-build
	docker run -d --rm -p 3000:3000 --name reto-devops reto-devops

docker-build:
	docker build -t reto-devops .

docker-stop:
	docker stop reto-devops || true

docker-clean: docker-stop
	docker rmi reto-devops

Reto2: Reto2-deploy

Reto2-deploy: compose-up

Reto2-remove: compose-rm

compose-up: compose-down compose-build
	docker-compose up -d

compose-down:
	docker-compose down

compose-build:
	docker-compose build --force-rm

compose-rm: compose-down
	docker-compose rm -f

Reto3: Reto3-deploy

Reto3-deploy: ci-start

Reto3-remove: ci-clean

ci-start: ci-stop
	docker build -t jenkins -f docker/jenkins/Dockerfile .
	docker run -d -u root --rm -it -p 8080:8080 --env JENKINS_ADMIN_ID=admin --env JENKINS_ADMIN_PASSWORD=sHgCgIBrw3QQP4ch7DTxH52DeQvm07uP --name jenkins -v /var/run/docker.sock:/var/run/docker.sock jenkins

ci-stop:
	docker stop jenkins || true

ci-clean: ci-stop
	docker rmi reto-devops

Reto4: Reto4-deploy

Reto4-deploy: k8s-deploy

Reto4-remove: kind-delete

k8s-deploy: k8s-release
	kubectl apply -f k8s/deployment.yaml
	kubectl apply -f k8s/service.yaml
	kubectl apply -f k8s/hpa.yaml

k8s-release: kind-create reto-devops-image

reto-devops-image:
	@test -f reto-devops.lock || docker build -t localhost:5000/reto-devops .
	@test -f reto-devops.lock || docker push localhost:5000/reto-devops
	@test -f reto-devops.lock || touch k8s-release.lock

kind-nginx: registry-create
	@test -f kind-cluster.lock || kind create cluster --config kind/kind-nginx.yaml
	@test -f kind-cluster.lock || kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml
	@test -f kind-cluster.lock || kubectl patch deployment metrics-server -n kube-system -p '{"spec":{"template":{"spec":{"containers":[{"name":"metrics-server","args":["--cert-dir=/tmp", "--secure-port=4443", "--kubelet-insecure-tls","--kubelet-preferred-address-types=InternalIP"]}]}}}}'
	@test -f kind-cluster.lock || docker network connect kind kind-registry > /dev/null 2>&1 &
	@touch kind-cluster.lock

kind-create: registry-create
	@test -f kind-cluster.lock || kind create cluster --config kind/kind-config.yaml
	@test -f kind-cluster.lock || kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml
	@test -f kind-cluster.lock || kubectl patch deployment metrics-server -n kube-system -p '{"spec":{"template":{"spec":{"containers":[{"name":"metrics-server","args":["--cert-dir=/tmp", "--secure-port=4443", "--kubelet-insecure-tls","--kubelet-preferred-address-types=InternalIP"]}]}}}}'
	@test -f kind-cluster.lock || docker network connect kind kind-registry > /dev/null 2>&1 &
	@touch kind-cluster.lock

kind-delete: registry-delete
	@kind delete cluster
	@rm -f *.lock

registry-create:
	@test -f registry.lock || docker run -d --restart=always -p 5000:5000 --name kind-registry registry:2
	@touch registry.lock

registry-delete:
	@docker rm kind-registry -f || true
	@rm -f registry.lock

Reto5: Reto5-deploy

Reto5-deploy: nginx-ingress

Reto5-remove: kind-delete

nginx-ingress: k8s-deploy nginx-image
	helm install nginx-ingress nginx-ingress --values nginx-ingress/values.yaml || helm upgrade nginx-ingress nginx-ingress --values nginx-ingress/values.yaml

ingress: kind-nginx
	$(MAKE) ingress-nginx
	@kind/ingress-nginx-controller.sh
	@kubectl apply -f k8s/ingress.yaml

ingress-nginx: k8s-deploy
	@kubectl get secret basic-auth &>/dev/null || kubectl create secret generic basic-auth --from-file=docker/webserver/inc/auth
	@kubectl get secret crt &>/dev/null || kubectl create secret tls crt --key docker/webserver/inc/localhost.key --cert docker/webserver/inc/localhost.crt
	@test -f ingress-nginx.lock || helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
	@test -f ingress-nginx.lock || helm repo update
	@test -f ingress-nginx.lock || helm install ingress-nginx ingress-nginx/ingress-nginx --set controller.extraArgs.default-ssl-certificate=default/crt
	@sleep 30
	@touch ingress-nginx.lock

nginx-image: k8s-deploy
	$(MAKE) -C docker/webserver nginx-image

Reto6: Reto6-deploy

Reto6-deploy: kind-nginx reto-devops-image
	$(MAKE) -C terraform deploy

Reto6-remove: kind-delete
