# Reto-DevOps CLM
![CLM Consoltores](./img/clm.png)

Este reto fue diseñado para mostrar tus habilidades DevOps. Este repositorio contiene una aplicación simple en NodeJs.

¡¡Te damos la bienvenida al desafío de CLM Consultores!! Si estás en este proceso, es porque nos interesa que puedas ser parte de nuestro equipo.
## Fase del proceso de selección:
Antes de comenzar con el desafío, te recomendamos leer las siguientes instrucciones:
  1. Es importante que realices el reto en forma tranquila (tendrás 3 días máximo para poder enviarlo). No te preocupes sino puedes completar todas las fases, para nosotros es importante que realices lo que consideras que tienes experiencia.
  2. En caso de que cumplas con el perfil técnico del cargo, la segunda fase del proceso de selección es una entrevista técnica, en dónde validaremos tus conocimientos y podrás saber con mayor detalle las funciones asociadas al cargo y sobre el equipo del área.
  3. Si continúas avanzando con nosotros, el próximo paso es una entrevista Psicolaboral con el área de Gestión de Personas y posteriormente la coordinación del ingreso a la empresa.

Una vez completado, no olvide notificar la solución **a Carla Santiago csantiago@clmconsultores.com**

Si tienes alguna duda, puedes escribir a Carla Santiago o enviar un correo a Gestión de Personas rrhh@clmconsultores.com

¡Te deseamos mucho éxito!

## La app
![NodeJs](./img/nodejs.png)

```bash
$ git clone https://gitlab.com/clm-public/reto-devops.git
Cloning into 'reto-devops'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
$ cd ./reto-devops

```
### Instalar Dependencias
```bash
$ npm install
npm WARN basicservice@1.0.0 No repository field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.2 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

added 530 packages from 308 contributors and audited 1203947 packages in 34.589s

21 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```
### Ejecutar Test
![Jest](./img/jest.jpg)

```bash
$ npm run test

> basicservice@1.0.0 test /basic-unit-test
> jest

 PASS  tests/sum.test.js
 PASS  tests/string.test.js

Test Suites: 2 passed, 2 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        5.655s
Ran all test suites.
```

### Ejecutar la app
```bash
$ node index.js
Example app listening on port 3000!
```
Podrá acceder a la API localmente en el puerto `3000`.

```bash
$ curl -s localhost:3000/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -s localhost:3000/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -s localhost:3000/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

**NT:** En nuestro equipo consideramos que cada aplicación debe venir bien documentada por parte del desarrollador para que el equipo de **DevOps** puede realizar los procesos de automatización de una manera mas eficaz.

## El reto comienza aquí
Tienes que hacer un **fork** de este repositorio para completar los siguientes retos en tu propia cuenta de `gitlab`. **Siéntete libre de resolver el reto que desees.** La cantidad de retos resueltos nos va a permitir valorar tus habilidades y realizar una **oferta en base a las mismas**.

1. Una vez completado, no olvide notificar la solución al **Carla Santiago (csantiago@clmconsultores.com).**
2. **La solución debe venir bien documentada, ten en cuenta que vamos a ejecutar la solución que nos envies para realizar la evaluación**
3. **Tiempo de solución 3 días**

Si tiene alguna duda, adelante, [abre un issue](https://gitlab.com/clm-public/reto-devops/issues) para hacer cualquier pregunta sobre cualquier reto.

### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

¿Qué pasa con los contenedores? En este momento **(2021)**, los contenedores son un estándar para implementar aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.

#### Respuesta:

##### Prerequisitos:

Se requiere instalar [docker](https://docs.docker.com/engine/install/) antes de ejecutar los siguientes comandos.

##### Crear contenedor:
```bash
$ docker build -t reto-devops .
```

##### Levantar contenedor:
```bash
$ docker run -d --rm -p 3000:3000 --name reto-devops reto-devops
```
**NT:** El contenedor correrá en background por lo que se devolverá la linea de comandos luego de ejecutar. La aplicación escuchara en el puerto local 3000.

##### Ejecutar pruebas:
```bash
$ curl -s localhost:3000/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -s localhost:3000/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -s localhost:3000/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

##### Detener Contenedor:
```bash
$ docker stop reto-devops
```

##### Opcional: Eliminar Imagen
Se puede utilizar este comando para liberar espacio.
```bash
$ docker rmi reto-devops
```

##### Extra: Utilizando make
Levantar el contenedor
```bash
$ make docker-start
```

Detener contendor
```bash
$ make docker-stop
```

Limpiar Imagen
```bash
$ make docker-clean
```

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

Una vez que haya dockerizado todos los componentes de la API *(aplicación de NodeJS)*, estarás listo para crear un archivo docker-compose, en nuestro equipo solemos usarlo para levantar ambientes de desarrollo antes de empezar a escribir los pipeline. Ya que la aplicación se ejecuta sin ninguna capa para el manejo de protocolo http, añace:

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

#### Respuesta:

##### Prerequisitos:

Se requiere instalar [docker-compose](https://docs.docker.com/compose/install/) antes de ejecutar los siguientes comandos.

##### Contruir contenedores:
```bash
$ docker-compose build --force-rm
```
**NT:** El el parámetro `--force-rm` fuerza eliminar todo los contenedores intermediarios. Si se requiere revisa más detalles consultar [docker-compose build](https://docs.docker.com/compose/reference/build/)

##### Levantar contenedores:
```bash
$ docker-compose up -d
```
**NT:** Se puede agregar el parámetro `--build` para contruir cada vez que se levanta, además se agrega el parametro `-d` por lo que el los contenedores correran en background. Si se requiere revisa más detalles consultar [docker-compose up](https://docs.docker.com/compose/reference/up/)

##### Ejecutar pruebas:
1. Habilitar https y redireccionar todo el trafico 80 --> 443

Se le agregar al `curl` el parámetro `-v` para obtener los *header* de *request* y *response*.

```bash
$ curl -v 127.0.0.1
*   Trying 127.0.0.1:80...
* Connected to 127.0.0.1 (127.0.0.1) port 80 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.21.0
< Date: Sun, 27 Jun 2021 18:15:25 GMT
< Content-Type: text/html
< Content-Length: 145
< Connection: keep-alive
< Location: https://127.0.0.1/
<
<html>
<head><title>302 Found</title></head>
<body>
<center><h1>302 Found</h1></center>
<hr><center>nginx/1.21.0</center>
</body>
</html>
* Connection #0 to host 127.0.0.1 left intact
```
2. Nginx que funcione como proxy reverso a nuesta app Nodejs

Se le puede agregar al `curl` los parámetros `-L` para seguir los redirect, `-k` para ignorar los certificados ya que son autofirmados y `-s` para no mostrar la barra de descarga.

```bash
$ curl -vksL 127.0.0.1 | jq
*   Trying 127.0.0.1:80...
* Connected to 127.0.0.1 (127.0.0.1) port 80 (#0)
> GET / HTTP/1.1
> Host: 127.0.0.1
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.21.0
< Date: Sun, 27 Jun 2021 18:21:02 GMT
< Content-Type: text/html
< Content-Length: 145
< Connection: keep-alive
< Location: https://127.0.0.1/
<
* Ignoring the response-body
* Connection #0 to host 127.0.0.1 left intact
* Issue another request to this URL: 'https://127.0.0.1/'
*   Trying 127.0.0.1:443...
* Connected to 127.0.0.1 (127.0.0.1) port 443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jun 27 08:20:36 2021 GMT
*  expire date: Jul 27 08:20:36 2021 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
> GET / HTTP/1.1
> Host: 127.0.0.1
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.21.0
< Date: Sun, 27 Jun 2021 18:21:02 GMT
< Content-Type: application/json; charset=utf-8
< Content-Length: 24
< Connection: keep-alive
< X-Powered-By: Express
< ETag: W/"18-wlNKgzbBjDUkquTlzfGN+kcWH0E"
<
* Connection #1 to host 127.0.0.1 left intact
{
  "msg": "ApiRest prueba"
}
```
Con esto podemos llegar al final de los redirect y que nos responda la ApiRest.

Igualmente se puede ejecutar el commando directo sin pasar por el redirect.

```bash
$ curl -vks https://127.0.0.1/ | jq
*   Trying 127.0.0.1:443...
* Connected to 127.0.0.1 (127.0.0.1) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jun 27 08:20:36 2021 GMT
*  expire date: Jul 27 08:20:36 2021 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
> GET / HTTP/1.1
> Host: 127.0.0.1
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.21.0
< Date: Sun, 27 Jun 2021 18:27:14 GMT
< Content-Type: application/json; charset=utf-8
< Content-Length: 24
< Connection: keep-alive
< X-Powered-By: Express
< ETag: W/"18-wlNKgzbBjDUkquTlzfGN+kcWH0E"
<
* Connection #0 to host 127.0.0.1 left intact
{
  "msg": "ApiRest prueba"
}
```

3. Asegurar el endpoint /private con auth_basic

```bash
$ curl -vksL localhost/private
*   Trying ::1:80...
* Connected to localhost (::1) port 80 (#0)
> GET /private HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.21.0
< Date: Sun, 27 Jun 2021 18:42:16 GMT
< Content-Type: text/html
< Content-Length: 145
< Connection: keep-alive
< Location: https://localhost/private
<
* Ignoring the response-body
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost/private'
*   Trying ::1:443...
* Connected to localhost (::1) port 443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.2 (IN), TLS handshake, Certificate (11):
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
* TLSv1.2 (IN), TLS handshake, Server finished (14):
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
* TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.2 (OUT), TLS handshake, Finished (20):
* TLSv1.2 (IN), TLS handshake, Finished (20):
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jun 27 08:20:36 2021 GMT
*  expire date: Jul 27 08:20:36 2021 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
> GET /private HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 401 Unauthorized
< Server: nginx/1.21.0
< Date: Sun, 27 Jun 2021 18:42:16 GMT
< Content-Type: text/html
< Content-Length: 179
< Connection: keep-alive
< WWW-Authenticate: Basic realm="Private Area"
<
<html>
<head><title>401 Authorization Required</title></head>
<body>
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx/1.21.0</center>
</body>
</html>
* Connection #1 to host localhost left intact
```

Si se carga la url desde el navegador obtenemos la autentificación básica

![nginx auth basic](./img/nginx-auth-basic.png)

Si utilizamos como usuario `user` y password `gHbhtg62JRCgrEwU7MtcGnvGOgYkHghK` podremos obtener el `private_token`

```bash
$ curl -ksL -uuser:gHbhtg62JRCgrEwU7MtcGnvGOgYkHghK http://127.0.0.1/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

###### Detener Contenedores:
```bash
$ docker-compose down
```

###### Opcional: Remover Contenedores
```bash
$ docker-compose rm -f
```

###### Extra: Utilizando make
Contruir contenedores
```bash
$ make compose-build
```

Levantar contenedores
```bash
$ make compose-up
```

Detener contenedores
```bash
$ make compose-down
```

Limpiar contenedores
```bash
$ make compose-rm
```

### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

Como buen ingeniero devops, conoces las ventajas de ejecutar tareas de forma automatizada. Hay algunos sistemas de cicd que pueden usarse para que esto suceda. Elige uno, travis-ci, gitlab-ci, circleci ... lo que quieras. Danos una tubería exitosa. **Gitlab-CI** es nuestra herramienta de uso diario por lo cual obtendras puntos extras si usas gitlab.

#### Respuesta:

##### GitLab CI:

Se implementó el archivo de configuración `.gitlab-ci.yml`, sin embargo la cuenta gratuita no permite ocuparlo por lo que quedo de ejemplo.

```
image: node:latest

stages:
    - build
    - test

build:
    stage: build
    script:
    - npm install

test:
    stage: test
    script:
    - npm run test
```

##### Jenkins:

Se creó el archivo de configuración `Jenkinsfile` que describe un *pipeline* para *Jenkins*

```
pipeline {
    agent any
    stages {
        stage('Unit') {
            steps {
                 sh 'make unit'
            }
        }
        stage('Build') {
            steps {
                sh 'make docker-build'
            }
        }
        stage('Deploy') {
            steps {
                sh 'make docker-stop || true && make docker-start'
            }
        }
        stage('Functional') {
            steps {
                sh 'curl -s 172.17.0.1:3000/ | jq .'
            }
        }
        stage('Destroy') {
            steps {
                sh 'make docker-clean || true'
            }
        }
    }
}
```

Para utilizarlo se creo una imagen de docker *Jenkins* funcional y que esta configurado con *configuration-as-code* plugin.

Con el archivo de configuración `docker/jenkins/inc/casc.yaml`, además, de tener los parametros básico incorpora un pipeline del repositorio [reto-devops](https://gitlab.com/jferrada86/reto-devops)

``` yaml
jenkins:
  securityRealm:
    local:
      allowsSignup: false
      users:
       - id: ${JENKINS_ADMIN_ID}
         password: ${JENKINS_ADMIN_PASSWORD}
  authorizationStrategy:
    globalMatrix:
      permissions:
        - "Overall/Administer:admin"
        - "Overall/Read:authenticated"
  remotingSecurity:
    enabled: true
jobs:
  - script: |
      multibranchPipelineJob('reto-devops') {
          branchSources {
              git {
                  id = '7911fec9-e5bf-4c99-9ccb-e2a07601bcc1'
                  remote('https://gitlab.com/jferrada86/reto-devops.git')
              }
          }
      }
security:
  queueItemAuthenticator:
    authenticators:
    - global:
        strategy: triggeringUsersAuthorizationStrategy
unclassified:
  location:
    url: http://localhost:8080/
```

Si se require instalar más plugin se puede editar el archivo `docker/jenkins/inc/plugins.txt`

###### Levantar Contenedor

Para simplificar la cantidad de comandos a ejecutar se integó con `make` la construccion del contenedo y la ajecución del mismo.

``` bash
$ make ci-start
```
**NT:** Este contenedor utiliza `Docker in Docker` para ejecutar los tests.

El contenedor ya este configurado y esta disponible en http://localhost:8080

![jenkins-login](./img/jenkins-login.png)

Si utilizamos como usuario `admin` y password `sHgCgIBrw3QQP4ch7DTxH52DeQvm07uP` podremos ingresar.

**NT:** Se puede cambiar el usuario y password editando el `Makefile` en la sección `ci-start`

Una vez en la interface ingresamos al repo `reto-devops`. Ahí estarán ya configurados todos los pipeline. Se mantendrá el branch `Retro3` para fines de este reto.

![jenkins-home](./img/jenkins-home.png)

![jenkins-reto-devops](./img/jenkins-reto-devops.png)

Finalmente se encontrará el pipeline en verde del Reto3.

![jenkins-pipeline](./img/jenkins-pipeline.png)

**NT:** Este *Jenkins* es 100% funcional, sin embargo, como se ejecuta de manera local no puede ser notificado cuando hay un nuevo commit,
por lo que si se requiere, se puede volver a ejecutar el *pipeline* para probar otro *commit* sin detener el contenedor.

Para detener el contendor se puede ejecutar:

``` bash
$ make ci-stop
```

### Reto 4. Deploy en kubernetes
![k8s](./img/k8s.png)

Ya que eres una máquina creando contenedores, ahora queremos ver tu experiencia en k8s. Use un sistema kubernetes para implementar la API. Recomendamos que uses herramientas como Minikube o Microk8s.

Escriba el archivo de implementación (archivo yaml) utilizalo para implementar su API (aplicación Nodejs).

* añade **Horizontal Pod Autoscaler** a la app NodeJS

#### Respuesta:

##### Prerequisitos:

Se requiere instalar [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) antes de ejecutar los siguientes comandos.

Se prefirió utilizar esta herramienta ya que utiliza *docker*, por lo que no ocupar tantos recursos como Minikube.

##### Crear Cluster Kind + Registry Local:

``` bash
$ docker run -d --restart=always -p 5000:5000 --name kind-registry registry:2
$ kind create cluster --config kind/kind-config.yaml
$ kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.3.6/components.yaml
$ kubectl patch deployment metrics-server -n kube-system -p '{"spec":{"template":{"spec":{"containers":[{"name":"metrics-server","args":["--cert-dir=/tmp", "--secure-port=4443", "--kubelet-insecure-tls","--kubelet-preferred-address-types=InternalIP"]}]}}}}'
$ docker network connect kind kind-registry > /dev/null 2>&1 &
```

##### Publicar Imagen en Registry Local:

``` bash
$ docker build -t localhost:5000/reto-devops .
$ docker push localhost:5000/reto-devops
```

##### Aplicar YAML:

Se crearon 3 archivos de configuración:

1. `k8s/deployment.yaml` contiene la configuración del deployment.

``` yaml
apiVersion: apps/v1

kind: Deployment

metadata:
  name: deployment
  labels:
    app: api

spec:
  replicas: 1
  selector:
    matchLabels:
      app: api
  template:
    metadata:
      labels:
        app: api
    spec:
      containers:
        - name: api
          image: localhost:5000/reto-devops
          ports:
            - containerPort: 3000
          resources:
            requests:
              cpu: "250m"
```

2. `k8s/service.yaml` contiene la configuración del service.

``` yaml
apiVersion: v1

kind: Service

metadata:
  name: app

spec:
  type: NodePort
  selector:
    app: api
  ports:
    - protocol: TCP
      port: 3000
      targetPort: 3000
      nodePort: 30000
```

3. `k8s/hpa.yaml` contiene la configuración del autoscaling.

``` yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: deployment
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: deployment
  minReplicas: 1
  maxReplicas: 10
  targetCPUUtilizationPercentage: 10
```

Para aplicar los 3 archivo ejecutar:

``` bash
$ kubectl apply -f k8s/deployment.yaml
$ kubectl apply -f k8s/service.yaml
$ kubectl apply -f k8s/hpa.yaml
```

##### Test Aplicación:
``` bash
$ curl -s http://localhost:3000 | jq
{
  "msg": "ApiRest prueba"
}
```

##### Obtener parámetros del cluster:
``` bash
$ kubectl get all
NAME                              READY   STATUS    RESTARTS   AGE
pod/deployment-5c75c74886-pgxsr   1/1     Running   0          28m

NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
service/kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP          32m
service/app          NodePort    10.96.33.152   <none>        3000:30000/TCP   30m

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/deployment   1/1     1            1           30m

NAME                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/deployment-5c75c74886   1         1         1       30m

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/deployment   Deployment/deployment   0%/10%    1         10        1          30m

```

##### Forzar carga al cluster:

``` bash
$ while true; do curl -s http://localhost:3000 &>/dev/null & done

```

**NT:** Se debe ejecutar en una segunda *shell*

##### Revisar autoscaling:

Después de aproximadamente 1 minuto ya comienza a escalar automáticamente el *cluster*.

``` bash
$ kubectl get all
NAME                              READY   STATUS    RESTARTS   AGE
pod/deployment-5c75c74886-4vffh   1/1     Running   0          16s
pod/deployment-5c75c74886-cd5s8   1/1     Running   0          31s
pod/deployment-5c75c74886-jb58m   1/1     Running   0          31s
pod/deployment-5c75c74886-kndkv   1/1     Running   0          16s
pod/deployment-5c75c74886-lrkvf   1/1     Running   0          16s
pod/deployment-5c75c74886-pgxsr   1/1     Running   0          32m
pod/deployment-5c75c74886-ssbg6   1/1     Running   0          31s

NAME                 TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
service/kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP          36m
service/app          NodePort    10.96.33.152   <none>        3000:30000/TCP   35m

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/deployment   7/7     7            7           35m

NAME                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/deployment-5c75c74886   7         7         7       35m

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/deployment   Deployment/deployment   70%/10%   1         10        7          35m

```

##### Opcional: Destruir Cluster

``` bash
$ kind delete cluster
$ docker rm kind-registry -f || true
```

##### Extra: Utilizando make

Se puede simplificar crear el *cluster* y *deployar* la aplicación ejecutando:

``` bash
$ make k8s-deploy
```

Automáticamente se eligieran que pasos ejecutar para crear el *cluster* y aplicar los archivos de configuración


### Reto 5. Construir Chart en helm y manejar trafico http(s)
![helm](./img/helm-logo-1.jpg)

Realmente el pan de cada día es crear, modificar y usar charts de helm. Este reto consiste en:

1. Diseñar un chart de helm con nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

##### Prerequisitos:

Se requiere instalar [kind](https://kind.sigs.k8s.io/docs/user/quick-start/#installation) antes de ejecutar los siguientes comandos.

Se prefirió utilizar esta herramienta ya que utiliza *docker*, por lo que no ocupar tantos recursos como Minikube.

Además se requiere instalar [helm](https://helm.sh/docs/intro/install/)

##### Crear Cluster Kind + Registry Local + Deploy + Servicio:

Para evitar repetir el Reto4 se partirá de la base que ya existe el cluster y se *deployó* la aplicacion y se creo su servicio.

Si se requiere volver a levantar el cluster se puede ejecutar:

``` bash
$ make Reto4
```

##### Crear Imagen nginx:

``` bash
$ cd docker/webserver
$ docker build -t localhost:5000/nginx .
$ docker push localhost:5000/nginx
$ cd -
```

##### Instalar nginx-ingress con Helm:

Se generó un chart de helm con la imagen *custom* de nginx en este repositirio que ya contaba con todas configuraciones previas.

``` bash
$ helm install nginx-ingress nginx-ingress --values nginx-ingress/values.yaml
```

**NT:**  *kind* esta configurado para conectar los puertos 80 y 443 del host a los puertos del cluster 30080 y 30443 por lo que no es necesario configurar el *ingress*

``` bash
$ kubectl get all
NAME                                READY   STATUS    RESTARTS   AGE
pod/deployment-5c75c74886-sxd6v     1/1     Running   0          14m
pod/nginx-ingress-bb5485755-8qrk2   1/1     Running   0          14m

NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
service/app             NodePort    10.96.177.126   <none>        3000:30000/TCP               14m
service/kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP                      16m
service/nginx-ingress   NodePort    10.96.21.23     <none>        80:30080/TCP,443:30443/TCP   14m

NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/deployment      1/1     1            1           14m
deployment.apps/nginx-ingress   1/1     1            1           14m

NAME                                      DESIRED   CURRENT   READY   AGE
replicaset.apps/deployment-5c75c74886     1         1         1       14m
replicaset.apps/nginx-ingress-bb5485755   1         1         1       14m

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/deployment   Deployment/deployment   0%/10%    1         10        1          14m

```
##### Pruebas:

1 - Habilitar https y redireccionar todo el trafico 80 --> 443

``` bash
$ curl -vksL localhost | jq
*   Trying ::1:80...
* connect to ::1 port 80 failed: Conexión rehusada
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET / HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.21.0
< Date: Tue, 29 Jun 2021 21:07:44 GMT
< Content-Type: text/html
< Content-Length: 145
< Connection: keep-alive
< Location: https://localhost/
<
* Ignoring the response-body
{ [145 bytes data]
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost/'
*   Trying ::1:443...
* connect to ::1 port 443 failed: Conexión rehusada
*   Trying 127.0.0.1:443...
* Connected to localhost (127.0.0.1) port 443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
} [5 bytes data]
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
} [512 bytes data]
* TLSv1.3 (IN), TLS handshake, Server hello (2):
{ [112 bytes data]
* TLSv1.2 (IN), TLS handshake, Certificate (11):
{ [766 bytes data]
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
{ [300 bytes data]
* TLSv1.2 (IN), TLS handshake, Server finished (14):
{ [4 bytes data]
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
} [37 bytes data]
* TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
} [1 bytes data]
* TLSv1.2 (OUT), TLS handshake, Finished (20):
} [16 bytes data]
* TLSv1.2 (IN), TLS handshake, Finished (20):
{ [16 bytes data]
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jun 27 08:20:36 2021 GMT
*  expire date: Jul 27 08:20:36 2021 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
} [5 bytes data]
> GET / HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
{ [5 bytes data]
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.21.0
< Date: Tue, 29 Jun 2021 21:07:44 GMT
< Content-Type: application/json; charset=utf-8
< Content-Length: 24
< Connection: keep-alive
< X-Powered-By: Express
< ETag: W/"18-wlNKgzbBjDUkquTlzfGN+kcWH0E"
<
{ [24 bytes data]
* Connection #1 to host localhost left intact
{
  "msg": "ApiRest prueba"
}
```

2 - Asegurar el endpoint /private con auth_basic

Primero sin usuario y password

``` bash
reto-devops git:(Reto5) ✗ curl -vksL localhost/private | jq .
*   Trying ::1:80...
* connect to ::1 port 80 failed: Conexión rehusada
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
> GET /private HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.21.0
< Date: Tue, 29 Jun 2021 21:10:44 GMT
< Content-Type: text/html
< Content-Length: 145
< Connection: keep-alive
< Location: https://localhost/private
<
* Ignoring the response-body
{ [145 bytes data]
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost/private'
*   Trying ::1:443...
* connect to ::1 port 443 failed: Conexión rehusada
*   Trying 127.0.0.1:443...
* Connected to localhost (127.0.0.1) port 443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
} [5 bytes data]
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
} [512 bytes data]
* TLSv1.3 (IN), TLS handshake, Server hello (2):
{ [112 bytes data]
* TLSv1.2 (IN), TLS handshake, Certificate (11):
{ [766 bytes data]
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
{ [300 bytes data]
* TLSv1.2 (IN), TLS handshake, Server finished (14):
{ [4 bytes data]
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
} [37 bytes data]
* TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
} [1 bytes data]
* TLSv1.2 (OUT), TLS handshake, Finished (20):
} [16 bytes data]
* TLSv1.2 (IN), TLS handshake, Finished (20):
{ [16 bytes data]
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jun 27 08:20:36 2021 GMT
*  expire date: Jul 27 08:20:36 2021 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
} [5 bytes data]
> GET /private HTTP/1.1
> Host: localhost
> User-Agent: curl/7.76.1
> Accept: */*
>
{ [5 bytes data]
* Mark bundle as not supporting multiuse
< HTTP/1.1 401 Unauthorized
< Server: nginx/1.21.0
< Date: Tue, 29 Jun 2021 21:10:44 GMT
< Content-Type: text/html
< Content-Length: 179
< Connection: keep-alive
< WWW-Authenticate: Basic realm="Private Area"
<
{ [179 bytes data]
* Connection #1 to host localhost left intact
```
Ahora con usuario `user` y password `gHbhtg62JRCgrEwU7MtcGnvGOgYkHghK` podremos obtener el `private_token`

``` bash
$ curl -vksL -uuser:gHbhtg62JRCgrEwU7MtcGnvGOgYkHghK localhost/private | jq .
*   Trying ::1:80...
* connect to ::1 port 80 failed: Conexión rehusada
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
* Server auth using Basic with user 'user'
> GET /private HTTP/1.1
> Host: localhost
> Authorization: Basic dXNlcjpnSGJodGc2MkpSQ2dyRXdVN010Y0dudkdPZ1lrSGdoSw==
> User-Agent: curl/7.76.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 302 Moved Temporarily
< Server: nginx/1.21.0
< Date: Tue, 29 Jun 2021 21:13:58 GMT
< Content-Type: text/html
< Content-Length: 145
< Connection: keep-alive
< Location: https://localhost/private
<
* Ignoring the response-body
{ [145 bytes data]
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost/private'
*   Trying ::1:443...
* connect to ::1 port 443 failed: Conexión rehusada
*   Trying 127.0.0.1:443...
* Connected to localhost (127.0.0.1) port 443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
} [5 bytes data]
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
} [512 bytes data]
* TLSv1.3 (IN), TLS handshake, Server hello (2):
{ [112 bytes data]
* TLSv1.2 (IN), TLS handshake, Certificate (11):
{ [766 bytes data]
* TLSv1.2 (IN), TLS handshake, Server key exchange (12):
{ [300 bytes data]
* TLSv1.2 (IN), TLS handshake, Server finished (14):
{ [4 bytes data]
* TLSv1.2 (OUT), TLS handshake, Client key exchange (16):
} [37 bytes data]
* TLSv1.2 (OUT), TLS change cipher, Change cipher spec (1):
} [1 bytes data]
* TLSv1.2 (OUT), TLS handshake, Finished (20):
} [16 bytes data]
* TLSv1.2 (IN), TLS handshake, Finished (20):
{ [16 bytes data]
* SSL connection using TLSv1.2 / ECDHE-RSA-AES256-GCM-SHA384
* ALPN, server accepted to use http/1.1
* Server certificate:
*  subject: CN=localhost
*  start date: Jun 27 08:20:36 2021 GMT
*  expire date: Jul 27 08:20:36 2021 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
* Server auth using Basic with user 'user'
} [5 bytes data]
> GET /private HTTP/1.1
> Host: localhost
> Authorization: Basic dXNlcjpnSGJodGc2MkpSQ2dyRXdVN010Y0dudkdPZ1lrSGdoSw==
> User-Agent: curl/7.76.1
> Accept: */*
>
{ [5 bytes data]
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.21.0
< Date: Tue, 29 Jun 2021 21:13:58 GMT
< Content-Type: application/json; charset=utf-8
< Content-Length: 60
< Connection: keep-alive
< X-Powered-By: Express
< ETag: W/"3c-cRwqN1Rv9e5tabqBVi4dqsdQZ1o"
<
{ [60 bytes data]
* Connection #1 to host localhost left intact
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

##### Extra: Instalando con Helm ingress-nginx

Destruimos todo lo anterior

``` bash
$ make Reto5-remove
```

Instalamos todo hasta el Reto4

``` bash
$ make kind-nginx Reto4
```
**NT:** la tarea de *make* *kind-nginx* genera un cluster kind con los puertos 80 y 443 del host conectado a los puerto 80 y 443 de cluster.

``` bash
$ kubectl get all                                             
NAME                              READY   STATUS    RESTARTS   AGE
pod/deployment-5c75c74886-9whhv   1/1     Running   0          3m20s

NAME                 TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)          AGE
service/app          NodePort    10.96.2.195   <none>        3000:30000/TCP   3m20s
service/kubernetes   ClusterIP   10.96.0.1     <none>        443/TCP          5m35s

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/deployment   1/1     1            1           3m20s

NAME                                    DESIRED   CURRENT   READY   AGE
replicaset.apps/deployment-5c75c74886   1         1         1       3m20s

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/deployment   Deployment/deployment   0%/10%    1         10        1          3m19s
```

**NT:** Si no se puede descargar la imagen ejecutar para conectar la red de kind con la de registry:

``` bash
$ docker network connect kind kind-registry > /dev/null 2>&1 &
```

Creamos los secretos

``` bash
$ kubectl create secret generic basic-auth --from-file=docker/webserver/inc/auth
$ kubectl create secret tls crt --key docker/webserver/inc/localhost.key --cert docker/webserver/inc/localhost.crt
```

``` bash
$ kubectl get secrets
NAME                  TYPE                                  DATA   AGE
basic-auth            Opaque                                1      13s
crt                   kubernetes.io/tls                     2      9s
default-token-58bg6   kubernetes.io/service-account-token   3      7m21s
```

**NT:** Se estan aprovechando los secretos ya creados, sin embargo, ingress-nginx ya trae un certificado por defecto.

Configuramos Helm e instalamos *ingress-nginx*

``` bash
$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
$ helm repo update
$ helm install ingress-nginx ingress-nginx/ingress-nginx --set controller.extraArgs.default-ssl-certificate=default/crt
```

**NT:** Acá se cambia el certificado por defecto por el que acabamos de crear.

Validamos que este corriendo el *service/ingress-nginx-controller*

``` bash
$ kubectl get all                                                                                                 
NAME                                           READY   STATUS    RESTARTS   AGE
pod/deployment-5c75c74886-9whhv                1/1     Running   0          5m35s
pod/ingress-nginx-controller-ddb9cd646-sbqbw   1/1     Running   0          45s

NAME                                         TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                      AGE
service/app                                  NodePort       10.96.2.195    <none>        3000:30000/TCP               5m35s
service/ingress-nginx-controller             LoadBalancer   10.96.185.86   <pending>     80:30547/TCP,443:31693/TCP   45s
service/ingress-nginx-controller-admission   ClusterIP      10.96.137.86   <none>        443/TCP                      45s
service/kubernetes                           ClusterIP      10.96.0.1      <none>        443/TCP                      7m50s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/deployment                 1/1     1            1           5m35s
deployment.apps/ingress-nginx-controller   1/1     1            1           45s

NAME                                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/deployment-5c75c74886                1         1         1       5m35s
replicaset.apps/ingress-nginx-controller-ddb9cd646   1         1         1       45s

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/deployment   Deployment/deployment   0%/10%    1         10        1          5m34s
```

Seteamos la EXTERNAL-IP

``` bash
$ IP=$(docker inspect $(docker ps | grep kind-control-plane | awk '{print $1}') | jq -r '.[].NetworkSettings.Networks.kind.IPAddress')
$ kubectl patch svc ingress-nginx-controller -p "{\"spec\": {\"type\": \"LoadBalancer\", \"externalIPs\":[\"$IP\"]}}"
```

Validamos nuevamente

``` bash
$ kubectl get all                                                                                                                     
NAME                                           READY   STATUS    RESTARTS   AGE
pod/deployment-5c75c74886-9whhv                1/1     Running   0          6m21s
pod/ingress-nginx-controller-ddb9cd646-sbqbw   1/1     Running   0          91s

NAME                                         TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                      AGE
service/app                                  NodePort       10.96.2.195    <none>        3000:30000/TCP               6m21s
service/ingress-nginx-controller             LoadBalancer   10.96.185.86   172.18.0.2    80:30547/TCP,443:31693/TCP   91s
service/ingress-nginx-controller-admission   ClusterIP      10.96.137.86   <none>        443/TCP                      91s
service/kubernetes                           ClusterIP      10.96.0.1      <none>        443/TCP                      8m36s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/deployment                 1/1     1            1           6m21s
deployment.apps/ingress-nginx-controller   1/1     1            1           91s

NAME                                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/deployment-5c75c74886                1         1         1       6m21s
replicaset.apps/ingress-nginx-controller-ddb9cd646   1         1         1       91s

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/deployment   Deployment/deployment   0%/10%    1         10        1          6m20s
```

Finalmente configuramos el *ingress*

``` bash
$ kubectl apply -f k8s/ingress.yaml
```
**NT:** El archivo *k8s/ingress.yaml* contiene ya las configuraciones de redirect y reglas de proxy por lo que no necesitamos levantar un nginx y crear las reglas.

``` yaml
apiVersion: networking.k8s.io/v1

kind: Ingress

metadata:
  annotations:
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
  name: nginx
  namespace: default

spec:
  tls:
  - hosts:
    - localhost
    secretName: crt
  rules:
  - host: localhost
    http:
      paths:
      - path: /
        pathType: Exact
        backend:
          service:
            name: app
            port:
              number: 3000

---

apiVersion: networking.k8s.io/v1

kind: Ingress

metadata:
  annotations:
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required'
  name: nginx-auth
  namespace: default

spec:
  tls:
  - hosts:
    - localhost
    secretName: crt
  rules:
  - host: localhost
    http:
      paths:
      - path: /private
        pathType: Exact
        backend:
          service:
            name: app
            port:
              number: 3000
```

Ahora ejecutamos una prueba

``` bash
curl -vksL -uuser:gHbhtg62JRCgrEwU7MtcGnvGOgYkHghK localhost/private | jq .                                                                                    
*   Trying ::1:80...
* connect to ::1 port 80 failed: Conexión rehusada
*   Trying 127.0.0.1:80...
* Connected to localhost (127.0.0.1) port 80 (#0)
* Server auth using Basic with user 'user'
> GET /private HTTP/1.1
> Host: localhost
> Authorization: Basic dXNlcjpnSGJodGc2MkpSQ2dyRXdVN010Y0dudkdPZ1lrSGdoSw==
> User-Agent: curl/7.76.1
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 308 Permanent Redirect
< Date: Tue, 29 Jun 2021 22:50:53 GMT
< Content-Type: text/html
< Content-Length: 164
< Connection: keep-alive
< Location: https://localhost/private
< 
* Ignoring the response-body
{ [164 bytes data]
* Connection #0 to host localhost left intact
* Issue another request to this URL: 'https://localhost/private'
*   Trying ::1:443...
* connect to ::1 port 443 failed: Conexión rehusada
*   Trying 127.0.0.1:443...
* Connected to localhost (127.0.0.1) port 443 (#1)
* ALPN, offering h2
* ALPN, offering http/1.1
* successfully set certificate verify locations:
*  CAfile: /etc/pki/tls/certs/ca-bundle.crt
*  CApath: none
} [5 bytes data]
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
} [512 bytes data]
* TLSv1.3 (IN), TLS handshake, Server hello (2):
{ [122 bytes data]
* TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
{ [19 bytes data]
* TLSv1.3 (IN), TLS handshake, Certificate (11):
{ [769 bytes data]
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
{ [264 bytes data]
* TLSv1.3 (IN), TLS handshake, Finished (20):
{ [52 bytes data]
* TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
} [1 bytes data]
* TLSv1.3 (OUT), TLS handshake, Finished (20):
} [52 bytes data]
* SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384
* ALPN, server accepted to use h2
* Server certificate:
*  subject: CN=localhost
*  start date: Jun 27 08:20:36 2021 GMT
*  expire date: Jul 27 08:20:36 2021 GMT
*  issuer: CN=localhost
*  SSL certificate verify result: self signed certificate (18), continuing anyway.
* Using HTTP2, server supports multi-use
* Connection state changed (HTTP/2 confirmed)
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
} [5 bytes data]
* Server auth using Basic with user 'user'
* Using Stream ID: 1 (easy handle 0x55623e11fbf0)
} [5 bytes data]
> GET /private HTTP/2
> Host: localhost
> authorization: Basic dXNlcjpnSGJodGc2MkpSQ2dyRXdVN010Y0dudkdPZ1lrSGdoSw==
> user-agent: curl/7.76.1
> accept: */*
> 
{ [5 bytes data]
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
{ [57 bytes data]
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
{ [57 bytes data]
* old SSL session ID is stale, removing
{ [5 bytes data]
* Connection state changed (MAX_CONCURRENT_STREAMS == 128)!
} [5 bytes data]
< HTTP/2 200 
< date: Tue, 29 Jun 2021 22:50:53 GMT
< content-type: application/json; charset=utf-8
< content-length: 60
< x-powered-by: Express
< etag: W/"3c-cRwqN1Rv9e5tabqBVi4dqsdQZ1o"
< strict-transport-security: max-age=15724800; includeSubDomains
< 
{ [60 bytes data]
* Connection #1 to host localhost left intact
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```
### Reto 6. Terraform
![docker](./img/tf.png)

En estos días en IaC no se habla de más nada que no sea terraform, en **CLM** ya nos encontramos con pipeline automatizados de Iac. El reto consiste en crear un modulo de terraform que nos permita crear un **rbac.authorization de tipo Role** que solo nos permita ver los pods de nuestro **namespace donde se encuentra al app Nodejs**

#### Respuesta:

##### Prerequisitos:

Se requiere instalar [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) antes de ejecutar los siguientes comandos.

##### Contruir cluster k8s + Registry + Images:
```bash
$ make kind-nginx reto-devops-image
```

##### Modulo:

Ya fue creado el modulo *reto6* dentro de terraform/modules/reto6.

``` bash
terraform
├── cluster-config
├── LICENSE
├── main.tf
├── Makefile
├── modules
│   └── reto6
│       ├── main.tf
│       ├── outputs.tf
│       └── variables.tf
├── outputs.tf
├── README.md
├── terraform.tfstate
├── terraform.tfstate.backup
└── variables.tf
```

En el *main.tf* se agregó la llamada al modulo y el nombre del rol.

```
module "reto6" {
  source        = "./modules/reto6"
  metadata_name = "pod-reader"
}
```

##### Deployar cluster con terraform y aplicar el modulo:

```bash
$ cd terraform
$ terraform init
$ terraform get
$ terraform plan
$ terraform apply -auto-approve
$ cd -
```

**NT:** El comando `terraform get` permite cargar el modulo local.


Para validar que el modulo creo el rol *pod-reader* ejecutar:

``` bash
$ kubectl get clusterroles -n app | grep pod-reader
pod-reader                                                             2021-06-30T03:16:20Z
```

### Reto 7. Automatiza el despliegue de los retos realizados
![docker](./img/make.gif)

Ya que hoy en día no queremos recordar recetas ni comandos, el reto consiste en **automatizar los retos en un Makefile**, considera especificar cuales son las dependencias necesarias para que tu Makefile se ejecute sin problemas.

**NT:** Se evaluará el orden en el cual se encuentre el repositorio, en el gran universo de aplicaciones que existe hoy en día el orden es un factor importante.

#### Respuesta:

Como ya se vió en los retos anteriores todos los comandos fueron automátizados con *make*, para ejecutar el deploy de un resto basta ejecutar `make RetoX-deploy` donde *X* corresponde al número de reto. Por ejemplo:

``` bash
make Reto1-deploy
```
O simplemente

``` bash
make Reto1
```

Para destruir todo lo que generó el reto se creo el comando `make RetoX-remove` donde *X* corresponde al número de reto. Por ejemplo:

``` bash
make Reto1-remove
```

