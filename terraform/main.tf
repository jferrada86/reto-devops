terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

resource "kubernetes_namespace" "reto" {
  metadata {
    name = "app"
  }
}

resource "kubernetes_deployment" "reto" {
  metadata {
    name      = "app"
    namespace = kubernetes_namespace.reto.metadata.0.name
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "app"
      }
    }
    template {
      metadata {
        labels = {
          app = "app"
        }
      }
      spec {
        container {
          image = "localhost:5000/reto-devops"
          name  = "app"
          port {
            container_port = 3000
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "reto" {
  metadata {
    name      = "app"
    namespace = kubernetes_namespace.reto.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.reto.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      node_port   = 30000
      port        = 3000
      target_port = 3000
    }
  }
}

module "reto6" {
  source        = "./modules/reto6"
  metadata_name = "pod-reader"
}
