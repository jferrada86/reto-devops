resource "kubernetes_cluster_role" "default" {
  metadata {
    name = var.metadata_name
  }
  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
  }
}
