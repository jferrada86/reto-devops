FROM node:16

MAINTAINER 'Juan Ferrada' juan.ferrada@gmail.com

WORKDIR /usr/src/app

COPY package*.json index.js ./

RUN npm install

EXPOSE 3000

USER node

CMD [ "node", "index.js" ]
